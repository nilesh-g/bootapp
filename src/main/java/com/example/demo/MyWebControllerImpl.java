package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MyWebControllerImpl {
	@Autowired
	private MathComponentImpl math;
	
	@GetMapping("/home")
	public String hello(Model model) {
		model.addAttribute("message", "Hello World!");
		return "home"; // view-name
	}
	
	@GetMapping("/math")
	public String math(Model model) {
		model.addAttribute("m", new MathObj());
		return "math"; // view-name
	}

	@PostMapping("/mathop")
	public String mathop(@ModelAttribute("m") MathObj m) {
		int res = 0;
		if(m.getOp().equals("+"))
			m.setResult(math.addNumbers(m.getNum1(), m.getNum2()));
		else if(m.getOp().equals("-"))
			m.setResult(math.subtractNumbers(m.getNum1(), m.getNum2()));
		else if(m.getOp().equals("*"))
			m.setResult(math.multiplyNumbers(m.getNum1(), m.getNum2()));
		else
			m.setResult(0);
		return "result"; // view-name
	}

}


