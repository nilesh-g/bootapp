package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyRestControllerImpl {
	@Autowired
	private MathComponentImpl math;
	
	@GetMapping("/hello")
	public String hello() {
		return "Hello World!";
	}
	
	@GetMapping("/add/{a}/{b}")
	public int add(@PathVariable("a") int a, @PathVariable("b") int b) {
		return math.addNumbers(a, b);
	}

	@GetMapping("/subtract/{a}/{b}")
	public int subtract(@PathVariable("a") int a, @PathVariable("b") int b) {
		return math.subtractNumbers(a, b);
	}

	@GetMapping("/multiply/{a}/{b}")
	public int multiply(@PathVariable("a") int a, @PathVariable("b") int b) {
		return math.multiplyNumbers(a, b);
	}
}
