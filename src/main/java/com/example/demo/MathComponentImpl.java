package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class MathComponentImpl {
	public int addNumbers(int num1, int num2) {
		return num1 + num2;
	}
	public int subtractNumbers(int num1, int num2) {
		return num1 - num2;
	}
	public int multiplyNumbers(int num1, int num2) {
		return num1 * num2;
	}
}
