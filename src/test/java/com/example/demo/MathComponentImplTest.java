package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MathComponentImplTest {
	@Autowired
	private MathComponentImpl m;
	
	@Test
	void testAddNumbers() {
		int result = m.addNumbers(12, 4);
		assertTrue(result == 16);
	}

	@Test
	void testSubtractNumbers() {
		int result = m.subtractNumbers(12, 4);
		assertTrue(result == 8);
	}

	@Test
	void testMultiplyNumbers() {
		int result = m.multiplyNumbers(12, 4);
		assertTrue(result == 48);
	}

}

